document.addEventListener("DOMContentLoaded", function () {
    const ipAddress = document.getElementById("ip-address");

    // Api that im using is da httpbin.org, if this api not working anymore find something else
    fetch('https://httpbin.org/ip')
        .then(response => response.json())
        .then(data => {
            const ip = data.origin;
            ipAddress.textContent = ip;
        })
        .catch(error => {
            ipAddress.textContent = "Error fetching IP address.";
        });

    ipAddress.addEventListener("click", function () {

        const textArea = document.createElement("textarea");
        textArea.value = ipAddress.textContent;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("copy");
        document.body.removeChild(textArea);

        ipAddress.textContent = "Copied! ✔️";
    });
});
